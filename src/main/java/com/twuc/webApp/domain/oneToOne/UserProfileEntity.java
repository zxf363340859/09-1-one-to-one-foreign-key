package com.twuc.webApp.domain.oneToOne;

import javax.persistence.*;

// TODO
//
// 请创建 UserEntity 和 UserProfileEntity 之间的 one-to-one 关系。并且确保 UserProfileEntity
// 的数据表结构如下：
//
// user_profile_entity
// +─────────────────+──────────────+──────────────────────────────+
// | column          | type         | additional                   |
// +─────────────────+──────────────+──────────────────────────────+
// | id              | bigint       | primary key, auto_increment  |
// | first_name      | varchar(32)  | not null                     |
// | last_name       | varchar(32)  | not null                     |
// | user_entity_id  | bigint       | not null                     |
// +─────────────────+──────────────+──────────────────────────────+
//
// <--start-
public class UserProfileEntity {
}
